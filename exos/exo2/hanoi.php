<?php

//Tours de Hanoi
//Regles: Faire passer les anneaux de A à C
//on ne bouge qu un seul disque à la fois (celui du haut)
//on pose un anneau uniquement sur un anneau plus grand
// logique: Utiliser la recursivite (l algo final fait 3 lignes)
// le nombre de mouvements minimal est 2^n-1 (n=nbre de disques)
// utiliser uen tige comme buffer
// faire passer la hauteur comme parametre pour verifier que le mouvement est valide

/**
 * 
 * [3,2,1] [] [] 
 * [3,2] [] [1] A - C
 * [3] [2] [1] A - B
 * [3] [2,1] [] C - B
 * [] [2,1] [3] A - C
 * [1] [2] [3] B - A
 * [1] [] [3,2] B - C
 * [] [] [3,2,1] A - C
 */

$nb_rings = 3;
$step = 0;

function Hanoi($nb_rings, $origin, $aux, $destination) {
    global $step;

    if($nb_rings === 0) return;    

    $step++;
    Hanoi($nb_rings-1, $origin, $destination, $aux);
    echo "move ring from ".$origin." to ".$destination."</br>";
    
    Hanoi($nb_rings-1, $aux, $origin, $destination);
}




Hanoi($nb_rings, "A", "B", "C");

echo "The Hanoi problem was resolved in ".$step." steps";

?>
