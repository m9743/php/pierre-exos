<body>
    <input type="text" id="name" name="subname" required minlength="1" maxlength="20" size="10">
    <div id="result"></div>

    <script>
        document.getElementById("name").addEventListener("keyup", (evt) => {
            let val = evt.target.value
            fetch('process.php?name=' + val)
                .then(function(response) {
                    response.text().then(function(data) {
                        console.log(data);
                        document.getElementById("result").innerHTML = data;
                    });
                })
        })
    </script>
</body>